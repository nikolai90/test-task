import apiGetCities from '@/api/cities'
import f from '@/utils/city-filters'

export default {
  namespaced: true,
  state: {
    cities: [],
    favCities: [],
    sQuery: ''
  },
  getters: {
    favCitiesIds: state => state.favCities.length ? state.favCities.map(c => c.id) : [],

    citiesFiltered(state, getters) {
      const cityFilter = (c) => {
        const res =
        f.cityWithoutFavs(c, getters.favCitiesIds) &&
        f.citySearchRes(c, state.sQuery)
        
        return res
      }
      
      let res = state.cities
      if (getters.favCitiesIds.length || state.sQuery) {
        res = state.cities.filter(cityFilter)
      }
      return res
    }
  },
  mutations: {
    M_SET_CITIES(state, payload) {
      state.cities = payload
    },
    M_ADD_CITY_TO_FAVORITES(state, city) {
      state.favCities.push(city)
    },
    M_REMOVE_CITY_FROM_FAVORITES(state, city) {
      const i = state.favCities.findIndex(c => c.id === city.id)
      if (i !== -1) state.favCities.splice(i, 1)
    },
    M_SET_QUERY(state, payload) {
      state.sQuery = payload
    }
  },
  actions: {
    async A_FETCH_CITIES({commit}) {
      const res = await apiGetCities()
      commit('M_SET_CITIES', res)
    }
  },
  modules: {
  }
}
