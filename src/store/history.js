export default {
  namespaced: true,
  state: {
    cityManagmentHistory: [],
    currentHistoryType: null
  },
  getters: {
    cityManagmentHistoryFiltered(state) {
      let records = state.cityManagmentHistory
      if (state.currentHistoryType) records = records.filter(r => r.type === state.currentHistoryType)
      return records
    }
  },
  mutations: {
    M_ADD_HISTORY_RECORD(state, payload) {
      state.cityManagmentHistory.push(payload)
    },
    M_SET_HISTORY_RECORDS(state, payload) {
      state.cityManagmentHistory = payload
    },
    M_SET_CURRENT_HISTORY_TYPE(state, payload) {
      state.currentHistoryType = payload
    }
  },
  modules: {
  }
}
