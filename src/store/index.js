import Vue from 'vue'
import Vuex from 'vuex'

import history from './history'
import cities from './cities'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    history,
    cities
  }
})
