import store from '@/store'

store.subscribe(m => {
  if(m.type === 'cities/M_ADD_CITY_TO_FAVORITES' || m.type === 'cities/M_REMOVE_CITY_FROM_FAVORITES') {
    const type = (m.type === 'cities/M_ADD_CITY_TO_FAVORITES') ? 'add' : 'remove'
    const payload = {
      ...m.payload,
      type,
      name: `${m.payload.name} ${type === 'add' ? 'Добавлен' : 'Удален'} ${new Date(Date.now()).toLocaleString()}`
    }
    
    store.commit('history/M_ADD_HISTORY_RECORD', payload)
  }

})