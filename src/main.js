import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueVirtualScroller from 'vue-virtual-scroller'
import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css'
import 'vue-virtual-scroller/dist/vue-virtual-scroller.css'
import './store/utils/subscriber'
Vue.config.productionTip = false
Vue.use(Vuesax)
Vue.use(VueVirtualScroller)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
