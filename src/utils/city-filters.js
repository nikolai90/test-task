export default {
  cityWithoutFavs: (c, favsIds) => !favsIds.includes(c.id),
  citySearchRes: (c, q) => c.name.toLowerCase().indexOf(q.toLowerCase()) !== -1 || c.items.filter(i => i.name.toLowerCase().indexOf(q.toLowerCase()) !== -1).length
}